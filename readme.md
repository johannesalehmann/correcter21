# Bedienungsanleitung

## Vorbereitung

1. Lade die Übungsdatein von Sciebo herunter und lege sie in einem beliebigen Ordner ab.
2. Lade die Punktetabelle des entsprechenden Blattes von Moodle herunter und speichere
sie im gleichen Ordner unter dem Namen "points.csv".
3. Erstelle einen weiteren Ordner und füge dort ebenfalls die Punkte-CSV ein (der Name ist hier egal).
Für die Team-Erkennungs-Heuristik kann man optinal die Punktetabellen der vergangenen Blätter
einfügen, dies ist jedoch nicht nötig (und funktioniert am Anfang auch nicht besonders gut).
4. Trage die entsprechenden Pfade in config.toml ein. Hier kann auch der PDF-Editor konfiguriert 
werden (Default ist Xournal++).

### Optional: Korrekturhinweis-Seite

Das Tool kann optional beim Export vor jeder Korrektur eine Zusatzseite einfügen. Hierfür wird
pdftk benötigt. Außerdem musst du eine entsprechende Seite entwerfen und an einem beliebigen 
Ort abspeichern. Trage den Pfad zu dieser Datei in config.toml ein.

Um diesen Schritt zu überspringen, reicht es, einen ungültigen Pfad in config.toml einzutragen.

## Benutzung

Starte das Tool mit "Correct21.exe path/to/config.toml". Wenn alles gut geht, erscheint danach das
Interface.

Der Korrekturprozess sieht in der Regel so aus (die Reihenfolge ist aber nicht notwendig):

1. Falls die Gruppe mehrere PDFs abgegeben hat, wähle die richtige Datei mit Shift+Pfeiltasten.
2. Öffne die markierte Datei mit [o]. Dies kann je nach PDF-Editor einige Sekunden dauern.
3. Trage die Namen der anderen Teilnehmer ein. Drücke hierfür [a] und tippe dann den Namen. Es kann
sowohl nach Vor- als auch nach Nachname gesucht werden.
4. Korrigiere die Übung.
5. Speichere das korrigierte PDF im gleichen Ordner unter beliebigem Namen. Falls der Editor
Projektdateien verwendet, kann diese ebenfalls im gleichen Ordner gespeichert werden.
6. Schließe Xournal. Falls die Datein nicht sichtbar sind, drücke [r]. Stelle sicher, dass
hinter der fertig korrigierten Datei [Output] steht. Diese Datei sehen die Studierenden später
im Moodle.
7. Trage die Punkte ein. Statt Komma muss hier Punkt verwendet werden.
8. Gehe zur nächsten Gruppe mit Pfeil nach unten.

Die Daten werden zwischengespeichert, also kann das Korrigieren zwischendurch unterbrochen werden.

Wenn alle Gruppen fertig korrigiert sind, kann der Export mit e gestartet werden. Dies dauert evtl.
einige Sekunden. Die Datein landen im Unterverzeichnis "export".

## Moodle-Upload

Für den Moodle-Upload müssen zunächst alle Ordner, die sich im export-Unterordner befinden, gezipt
werden (nicht aber die Punkte-Datei). Dieser Zip-Ordner kann im Moodle unter
"Übungsblatt n"->"Alle Abgaben anzeigen"->"Bewertungsvorgang"->"Mehrere Feedbackdatein in einer
Zip-Datei hochladen" hochgeladen werden. Wenn alles gut geht, sollte jedes Gruppenmitglied eine
Datei bekommen.

Die Punkte können über "Bewertungsvorgang"->"Bewertungstabelle hochladen" eingetragen werden.
Hierbei muss der Haken bei "Update von Datensätzen zulassen, die seit dem letzten Upload angepasst
wurden" gesetzt werden.

*Achtung*: Falls durch LaTeX-Bonus das Punktemaximum überschritten wird, werden diese Punkte nicht
eingetragen, aber es gibt keine Warnung. Ich glaube, daran ist Moodle schuld :(

## Minitests

Für den Minitest-Modus muss das Tool mit "Correct21.exe minitest path/to/points.csv" gestartet
werden, wobei points.csv die zum entsprechenden Minitest gehörende csv ist. Man trägt dann immer
abwechselnd Namen und Punkte ein. Mit Pfeiltasten können Vorschläge ausgewählt werden und
falsche Einträge gelöscht.

Am Ende exportiert man mit Strg+e. Die exportierte Datei landet im gleichen Verzeichnis wie points.csv.

*Achtung*: Die Minitest-Korrektur wird nicht zwischengespeichert. Falls das Programm beendet wird,
gehen die eingetragenen Punkte verloren.
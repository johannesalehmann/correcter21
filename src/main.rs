mod terminal;

use std::io;
use std::path::Path;

fn main() -> Result<(), io::Error> {
    let args = std::env::args().collect::<Vec<_>>();
    let mut minitest_mode = false;
    if args.len() >= 2 && args[1] == "minitest" {
        minitest_mode = true;
    }

    if minitest_mode {
        if args.len() < 3 {
            println!("Usage: ./Correct21.exe minitest path/to/points.csv");
        } else {
            let point_file = Path::new(&args[2]);
            let mut minitester = terminal::minitest_mode::Minitester::new(point_file);
            minitester.input_loop();
        }
    } else {
        if args.len() < 2 {
            println!("Usage: ./Correct21.exe path/to/config.toml");
        } else {
            let config_path = Path::new(&args[1]);
            let mut correcter = terminal::Correcter::new(config_path);
            correcter.input_loop();
        }
    }

    Ok(())
}

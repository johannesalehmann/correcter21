use serde::Deserialize;
use std::path::Path;

#[derive(Deserialize)]
pub struct Config {
    pub operating_directory: String,
    pub points_directory: String,
    pub pdf_editor: String,
    pub pdf_editor_extension: String,
    pub correction_note_file: String,
}

impl Config {
    pub fn from_file(path: &Path) -> Self {
        let config_file = std::fs::read_to_string(path).expect("Failed to open config file");
        let config: Config =
            toml::from_str(&config_file).expect("Config file does not correspond to format");
        config
    }
}

use crate::terminal::config::Config;
use crate::terminal::point_sheets::{Person, PointSheets};
use crate::terminal::InputMode;
use serde::{Deserialize, Serialize};
use std::fs;
use std::io::Stdout;
use std::path::{Path, PathBuf};
use tui::backend::CrosstermBackend;
use tui::layout::{Constraint, Direction, Layout, Rect};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, List, ListItem, ListState, Paragraph};
use tui::Frame;

#[derive(Serialize, Deserialize)]
pub struct Member {
    pub name: String,
    pub id: String,
    pub directory: std::path::PathBuf,

    pub known_files: Vec<String>,
    pub selected_file: Option<String>,
    pub output_file: Option<String>,

    pub other_members: Vec<super::Person>,
    pub selected_name_option: usize,

    pub points: Option<f32>, // Should hopefully provide exact representation for all reasonable point numbers
}
impl Member {
    pub fn from_dir(parent: &std::path::Path, directory: std::path::PathBuf) -> Option<Self> {
        let local_title = directory
            .strip_prefix(parent)
            .unwrap()
            .to_str()
            .expect("Only paths that are valid unicode are supported")
            .to_string();

        let name_end = local_title.find("_")?;
        let name = local_title[0..name_end].to_string();
        let id_end = local_title[name_end + 1..].find("_")?;
        let id = local_title[name_end + 1..name_end + 1 + id_end].to_string();

        let known_files = Self::get_files(&directory);
        let mut selected_file = None;
        for file in &known_files {
            if file.to_lowercase().ends_with(".pdf") {
                selected_file = Some(file.clone());
            }
        }

        Some(Self {
            name,
            id,
            directory,

            known_files,
            selected_file,
            output_file: None,

            other_members: Vec::new(),
            selected_name_option: 0,

            points: None,
        })
    }

    fn get_files(path: &PathBuf) -> Vec<String> {
        let mut res = Vec::new();
        for dir in fs::read_dir(path).expect("Could not read contents of group directory") {
            if let Ok(dir) = dir {
                res.push(
                    dir.file_name()
                        .into_string()
                        .expect("Group directory contained non-unicode character in file name"),
                );
            }
        }
        res
    }

    pub fn update_known_files(&mut self, config: &Config) {
        let new_known_files = Self::get_files(&self.directory);
        for new_file in &new_known_files {
            if !self.known_files.contains(new_file) {
                if self.output_file.is_none() && new_file.to_lowercase().ends_with(".pdf") {
                    self.output_file = Some(new_file.to_string());
                }
                if new_file
                    .to_lowercase()
                    .ends_with(&config.pdf_editor_extension)
                {
                    self.selected_file = Some(new_file.to_string());
                }
            }
        }

        if self.selected_file.is_some()
            && !new_known_files.contains(self.selected_file.as_ref().unwrap())
        {
            self.selected_file = None;
        }
        if self.output_file.is_some()
            && !new_known_files.contains(self.output_file.as_ref().unwrap())
        {
            self.output_file = None;
        }
        self.known_files = new_known_files;
    }

    pub fn update(&mut self, config: &Config) {
        self.update_known_files(config);
    }

    pub fn draw(
        &self,
        f: &mut Frame<CrosstermBackend<Stdout>>,
        area: Rect,
        input_mode: InputMode,
        typed_string: &str,
        point_sheet: &super::PointSheets,
    ) {
        let chunks = Layout::default()
            .horizontal_margin(1)
            .vertical_margin(1)
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(self.known_files.len() as u16 + 7),
                Constraint::Length(5),
                Constraint::Length(7),
            ])
            .split(area);

        self.draw_file_list(f, chunks[0]);
        self.draw_points(f, chunks[1], input_mode, typed_string);
        self.draw_group_members(f, chunks[2], input_mode, typed_string, point_sheet);
    }

    pub fn draw_file_list(&self, f: &mut Frame<CrosstermBackend<Stdout>>, area: Rect) {
        let files: Vec<ListItem> = self
            .known_files
            .iter()
            .map(|m| {
                if Some(m) == self.output_file.as_ref() {
                    ListItem::new(format!("{} [Output]", m))
                } else {
                    ListItem::new(m.clone())
                }
            })
            .chain(
                [
                    " ",
                    "  open selected: [o]",
                    "change selected: Shift+Arrows",
                    "  change output: Ctrl+Arrows",
                    "        refresh: [r]",
                    " normalise name: [n]",
                ]
                .map(|m| ListItem::new(m)),
            )
            .collect();
        let file_list = List::new(files)
            .block(Block::default().title(" Files ").borders(Borders::ALL))
            .style(Style::default().fg(Color::White))
            .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
            .highlight_symbol("> ");
        let mut file_list_state = ListState::default();
        file_list_state.select(
            self.known_files
                .iter()
                .position(|file| Some(file) == self.selected_file.as_ref()),
        );

        f.render_stateful_widget(file_list, area, &mut file_list_state);
    }
    pub fn draw_points(
        &self,
        f: &mut Frame<CrosstermBackend<Stdout>>,
        area: Rect,
        input_mode: InputMode,
        typed_string: &str,
    ) {
        let text = match input_mode {
            InputMode::EnteringPoints => {
                format!(
                    "Current Points: {}_\n\nConfirm: [Enter], Cancel: [Esc]",
                    typed_string
                )
            }
            _ => {
                let first_line = match self.points {
                    Some(points) => format!("Current Points: {}", points),
                    None => "No points entered".to_string(),
                };
                format!("{}\n\nChange points: [p]", first_line)
            }
        };
        let points = Paragraph::new(text)
            .block(Block::default().title(" Grading ").borders(Borders::ALL))
            .style(Style::default().fg(Color::White));
        f.render_widget(points, area);
    }

    pub fn draw_group_members(
        &self,
        f: &mut Frame<CrosstermBackend<Stdout>>,
        area: Rect,
        input_mode: InputMode,
        typed_string: &str,
        point_sheet: &super::PointSheets,
    ) {
        let add_text = if input_mode == InputMode::EnteringMembers {
            let suggested = self.get_suggested_persons(typed_string, point_sheet);
            format!(
                "Search: {}_\n{}",
                typed_string,
                suggested
                    .iter()
                    .take(5)
                    .enumerate()
                    .map(|(index, person)| {
                        if index == self.selected_name_option {
                            format!("> {}", person.name)
                        } else {
                            format!("  {}", person.name)
                        }
                    })
                    .collect::<Vec<String>>()
                    .join("\n")
            )
        } else {
            if self.other_members.is_empty() {
                "[a] Add Members".to_string()
            } else {
                format!(
                    "[a] Add Members\n[x] Remove \'{}\'",
                    self.other_members[self.other_members.len() - 1].name
                )
            }
        };
        let mut members = format!(
            "  {}{}\n\n{}",
            self.name.clone(),
            [""].into_iter()
                .chain(self.other_members.iter().map(|member| member.name.as_str()))
                .collect::<Vec<_>>()
                .join("\n  "),
            add_text
        );

        let points = Paragraph::new(members)
            .block(
                Block::default()
                    .title(" Group Members ")
                    .borders(Borders::ALL),
            )
            .style(Style::default().fg(Color::White));
        f.render_widget(points, area);
    }

    pub fn get_suggested_persons(
        &self,
        typed_string: &str,
        point_sheet: &PointSheets,
    ) -> Vec<Person> {
        let all_persons = point_sheet.identify_teammates(&self.name);
        let suggested = all_persons
            .iter()
            .filter(|p| p.matches_search(typed_string) && !self.has_member(&p.id))
            .map(|p| p.clone())
            .collect::<Vec<_>>();
        suggested
    }

    pub fn has_member(&self, id: &str) -> bool {
        self.other_members.iter().any(|member| member.id == id)
    }

    pub fn get_self_and_other_members(&self) -> Vec<Person> {
        let mut res = vec![Person {
            name: self.name.clone(),
            id: self.id.clone(),
            certainty: 0.0,
            matriculation_number: None,
        }];
        for other in &self.other_members {
            res.push(other.clone());
        }
        res
    }

    pub fn increment_selected_file(&mut self) {
        self.change_selected_file(1);
    }
    pub fn decrement_selected_file(&mut self) {
        self.change_selected_file(self.known_files.len() - 1);
    }
    fn change_selected_file(&mut self, delta_index: usize) {
        let current_index = if let Some(current_file) = &self.selected_file {
            self.known_files
                .iter()
                .position(|s| s == current_file)
                .unwrap_or(0)
        } else {
            0
        };
        let new_index = (current_index + delta_index) % self.known_files.len();
        self.selected_file = Some(self.known_files[new_index].clone());
    }

    pub fn increment_output_file(&mut self) {
        self.change_output_file(1);
    }
    pub fn decrement_output_file(&mut self) {
        self.change_output_file(self.known_files.len() - 1);
    }
    fn change_output_file(&mut self, delta_index: usize) {
        let current_index = if let Some(current_file) = &self.output_file {
            self.known_files
                .iter()
                .position(|s| s == current_file)
                .unwrap_or(0)
        } else {
            0
        };
        let new_index = (current_index + delta_index) % self.known_files.len();
        self.output_file = Some(self.known_files[new_index].clone());
    }

    pub fn open_selected(&self, config: &Config) {
        if let Some(selected) = &self.selected_file {
            let path = self.directory.join(selected);
            let output = std::process::Command::new(&config.pdf_editor)
                .arg(path)
                .spawn()
                .expect("Failed to launch pdf editor");
        }
    }

    pub fn store_in_file(&self, directory: &Path) {
        let filename = directory.join(Path::new(&self.id));
        std::fs::write(filename, serde_json::to_string(&self).unwrap()).unwrap();
    }

    pub fn load_from_file(directory: &Path, id: &str) -> Option<Self> {
        let filename = directory.join(Path::new(id));
        if let Ok(content) = std::fs::read_to_string(filename) {
            Some(serde_json::from_str(&content).unwrap())
        } else {
            None
        }
    }
}

use super::PointSheet;
use crate::terminal::point_sheets::{Person, PointSheets};
use crossterm::event::{Event, KeyCode, KeyEvent, KeyModifiers};
use crossterm::execute;
use crossterm::terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen};
use std::io;
use std::io::Stdout;
use std::path::{Path, PathBuf};
use tui::backend::CrosstermBackend;
use tui::widgets::Paragraph;
use tui::Terminal;

#[derive(Clone)]
struct Entry {
    id: String,
    name: String,
    matriculation_number: String,
    points: f32,
}

#[derive(Eq, PartialEq)]
enum InputMode {
    EnteringName,
    EnteringPoints,
}

pub struct Minitester {
    point_sheets: PointSheets,
    points_file: PathBuf,
    terminal: Terminal<CrosstermBackend<Stdout>>,

    entries: Vec<Entry>,

    selected_line: usize,

    input_mode: InputMode,
    typed_string: String,
    partial_entry: Option<Entry>,
    suggestions: Vec<String>,
    messages: Option<String>,
}

impl Minitester {
    pub fn new(point_file: &Path) -> Self {
        enable_raw_mode().expect("Failed to enable terminal raw mode");
        let mut stdout = io::stdout();
        execute!(stdout, EnterAlternateScreen).unwrap();
        let backend = CrosstermBackend::new(stdout);
        let terminal = Terminal::new(backend).expect("Failed to build terminal");

        let point_sheet = PointSheet::from_file(point_file);

        Self {
            point_sheets: PointSheets {
                sheets: vec![point_sheet],
            },
            points_file: point_file.to_path_buf(),
            terminal,
            entries: vec![],
            selected_line: 0,
            input_mode: InputMode::EnteringName,
            typed_string: "".to_string(),
            partial_entry: None,
            suggestions: vec![],
            messages: None,
        }
    }

    pub fn input_loop(&mut self) {
        let mut abort = false;
        while !abort {
            self.draw();
            let mut redraw = false;
            while !redraw && !abort {
                match crossterm::event::read() {
                    Ok(event) => match event {
                        Event::Key(key) => {
                            if key.code == KeyCode::Char('c')
                                && key.modifiers == KeyModifiers::CONTROL
                            {
                                abort = true;
                                redraw = true;
                            } else {
                                match self.input_mode {
                                    InputMode::EnteringPoints => {
                                        self.process_enter_points(key);
                                        redraw = true;
                                    }

                                    InputMode::EnteringName => {
                                        self.process_enter_name(key);
                                        redraw = true;
                                    }
                                }
                            }
                        }
                        Event::Mouse(_) => {}
                        Event::Resize(_, _) => {
                            redraw = true;
                        }
                    },
                    Err(err) => {
                        panic!("Error reading input event: {}", err)
                    }
                }
            }
        }

        disable_raw_mode().expect("Failed to disable raw mode");
        execute!(
            self.terminal.backend_mut(),
            crossterm::terminal::LeaveAlternateScreen,
        )
        .expect("Failed to leave alternate screen");
        self.terminal.show_cursor().expect("Failed to show cursor");
    }

    fn process_enter_points(&mut self, key: KeyEvent) {
        self.process_typed_string(key);
        if key.code == KeyCode::Esc {
            self.input_mode = InputMode::EnteringName;
            self.typed_string = "".to_string();
            self.partial_entry = None;
        }
        if key.code == KeyCode::Enter {
            if let Ok(points) = self.typed_string.parse::<f32>() {
                let mut partial_entry = self.partial_entry.clone().unwrap();
                self.partial_entry = None;
                partial_entry.points = points;
                self.entries.push(partial_entry);

                self.typed_string = "".to_string();
                self.input_mode = InputMode::EnteringName;
                self.selected_line += 1;
            }
        }
    }

    fn process_enter_name(&mut self, key: KeyEvent) {
        if self.messages.is_some() {
            if key.code == KeyCode::Enter {
                self.messages = None;
            }
            return;
        }

        if self.selected_line == self.entries.len() {
            self.process_typed_string(key);

            self.suggestions.clear();
            if !self.typed_string.is_empty() {
                let selected = self.get_suggested_persons(&self.typed_string);
                for select in &selected {
                    self.suggestions.push(format!(
                        "{} {}",
                        select.matriculation_number.as_ref().unwrap(),
                        select.name
                    ));
                }
            }
        }

        if key.code == KeyCode::Up || key.code == KeyCode::Down {
            let length = self.entries.len() + self.suggestions.len() + 1;
            let delta = if key.code == KeyCode::Up {
                length - 1
            } else {
                1
            };
            self.selected_line += delta;
            self.selected_line %= length;
        }

        if key.code == KeyCode::Enter || (key.code == KeyCode::Tab && self.suggestions.len() == 1) {
            if self.suggestions.len() == 1 && self.selected_line == self.entries.len() {
                self.selected_line += 1;
            }
            if self.selected_line > self.entries.len() {
                let selected = self.get_suggested_persons(&self.typed_string);
                let person = &selected[self.selected_line - self.entries.len() - 1];
                self.partial_entry = Some(Entry {
                    id: person.id.clone(),
                    name: person.name.clone(),
                    matriculation_number: person.matriculation_number.as_ref().unwrap().clone(),
                    points: 0.0,
                });
                self.suggestions.clear();
                self.input_mode = InputMode::EnteringPoints;
                self.typed_string = "".to_string();
                self.selected_line = self.entries.len();
            }
        }

        if key.code == KeyCode::Delete {
            if self.selected_line < self.entries.len() {
                self.entries.remove(self.selected_line);
                if self.selected_line > 0 {
                    self.selected_line -= 1;
                }
            }
        }

        if key.code == KeyCode::Char('e') && key.modifiers == KeyModifiers::CONTROL {
            let messages = self.export();
            if !messages.is_empty() {
                self.messages = Some(messages);
            }
        }
    }

    fn process_typed_string(&mut self, key: KeyEvent) {
        if let KeyCode::Char(c) = key.code {
            self.typed_string += &c.to_string();
        }
        if key.code == KeyCode::Backspace {
            let mut dist = 1;
            loop {
                if self.typed_string.len() >= dist {
                    if self
                        .typed_string
                        .is_char_boundary(self.typed_string.len() - dist)
                    {
                        break;
                    }
                    dist += 1;
                } else {
                    break;
                }
            }
            if self.typed_string.len() >= dist {
                self.typed_string = self.typed_string[..self.typed_string.len() - dist].to_string();
            }
        }
    }

    pub fn get_suggested_persons(&self, typed_string: &str) -> Vec<Person> {
        let all_persons = self.point_sheets.identify_teammates("Minitest_Tool_Bypass");
        let suggested = all_persons
            .iter()
            .filter(|p| p.matches_search(typed_string) && !self.has_entry(&p.id))
            .map(|p| p.clone())
            .collect::<Vec<_>>();
        suggested
    }

    pub fn has_entry(&self, id: &str) -> bool {
        self.entries.iter().any(|entry| entry.id == id)
    }

    pub fn draw(&mut self) {
        self.terminal
            .draw(|f| {
                let typing_line = if self.input_mode == InputMode::EnteringName {
                    if self.selected_line == self.entries.len() {
                        if self.typed_string.is_empty() {
                            "> _                (type to search)".to_string()
                        } else {
                            format!("> {}_", self.typed_string)
                        }
                    } else {
                        if self.typed_string.is_empty() {
                            "     <select this line to search for students>".to_string()
                        } else {
                            format!("  {}", self.typed_string)
                        }
                    }
                } else {
                    let partial_entry = self.partial_entry.as_ref().unwrap();
                    if self.selected_line == self.entries.len() {
                        format!(
                            "> {} {}: {}_",
                            partial_entry.matriculation_number,
                            partial_entry.name,
                            self.typed_string
                        )
                    } else {
                        format!(
                            "  {} {}: {}",
                            partial_entry.matriculation_number,
                            partial_entry.name,
                            self.typed_string
                        )
                    }
                };

                let mut text = self
                    .entries
                    .iter()
                    .enumerate()
                    .map(|(index, entry)| {
                        let (selected, postfix) = if index == self.selected_line {
                            ("> ".to_string(), " ([Del]: remove)".to_string())
                        } else {
                            ("  ".to_string(), "".to_string())
                        };
                        format!(
                            "{}{} {}: {} {}",
                            selected, entry.matriculation_number, entry.name, entry.points, postfix
                        )
                    })
                    .collect::<Vec<_>>()
                    .join("\n");
                text = format!(
                    "[Ctrl] + [e]: Export Points\n\n{}\n{}\n{}",
                    text,
                    typing_line,
                    self.suggestions
                        .iter()
                        .enumerate()
                        .map(|(index, suggestion)| {
                            let selected = if index + 1 + self.entries.len() == self.selected_line {
                                ">   ".to_string()
                            } else {
                                "    ".to_string()
                            };
                            format!("{}{}", selected, suggestion)
                        })
                        .collect::<Vec<_>>()
                        .join("\n")
                );

                if let Some(messages) = &self.messages {
                    text = format!("Message: \n{}\n\nPress Enter to continue", messages);
                }

                let paragraph = Paragraph::new(text.as_str());
                f.render_widget(paragraph, f.size());
            })
            .unwrap();
    }

    pub fn export(&self) -> String {
        let point_file_lines = std::fs::read_to_string(&self.points_file)
            .unwrap()
            .lines()
            .map(|line| line.to_string())
            .collect::<Vec<_>>();
        let mut point_file = point_file_lines[0].to_string();

        let mut messages = "".to_string();
        for entry in &self.entries {
            let search_pattern = format!("Teilnehmer/in{}", entry.id);
            let mut found_pattern = false;
            for point_file_line in &point_file_lines {
                if point_file_line.starts_with(&search_pattern) {
                    let new_row =
                        super::Correcter::enter_points_into_row(point_file_line, entry.points);
                    point_file = format!("{}\n{}", point_file, new_row);
                }
            }
            if !found_pattern {
                messages = format!(
                    "{}No entry for {} (id: {}) in point file\n",
                    messages, entry.name, entry.id
                );
            }
        }

        let new_file = self.points_file.with_extension("exported.csv");
        std::fs::write(&new_file, point_file).unwrap();

        messages = "Export complete".to_string();
        messages
    }
}

mod config;
mod member;
pub mod minitest_mode;
mod point_sheets;

use crate::terminal::config::Config;
use crate::terminal::member::Member;
use crate::terminal::point_sheets::{Person, PointSheet, PointSheets};
use crossterm::event::{Event, KeyCode, KeyEvent, KeyModifiers};
use crossterm::execute;
use crossterm::terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen};
use std::fs;
use std::io;
use std::io::Stdout;
use std::path::Path;
use tui::backend::CrosstermBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, List, ListItem, ListState, Paragraph};
use tui::Terminal;

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum InputMode {
    Normal,
    EnteringPoints,
    EnteringMembers,
}

pub struct Correcter {
    terminal: Terminal<CrosstermBackend<Stdout>>,

    selected_member: usize,
    members: Vec<Member>,

    point_sheets: PointSheets,

    input_mode: InputMode,
    typed_string: String,

    config: Config,

    messages: Option<String>,
}

impl Correcter {
    pub fn new(path_to_config: &Path) -> Self {
        enable_raw_mode().expect("Failed to enable terminal raw mode");
        let mut stdout = io::stdout();
        execute!(stdout, EnterAlternateScreen).unwrap();
        let backend = CrosstermBackend::new(stdout);
        let terminal = Terminal::new(backend).expect("Failed to build terminal");

        let config = Config::from_file(path_to_config);

        let members = Self::load_members(Path::new(&config.operating_directory));

        let point_sheets = PointSheets::from_directory(Path::new(&config.points_directory));

        Self {
            terminal,

            selected_member: 0,
            members,

            point_sheets,

            input_mode: InputMode::Normal,
            typed_string: "".to_string(),

            config,

            messages: None,
        }
    }

    pub fn load_members(path: &Path) -> Vec<Member> {
        let mut res = Vec::new();
        for dir in fs::read_dir(path).expect("Could not read correction file directory") {
            if let Ok(dir) = dir {
                if let Some(member) = Member::from_dir(path, dir.path()) {
                    if let Some(loaded_member) = Member::load_from_file(path, &member.id) {
                        res.push(loaded_member);
                    } else {
                        res.push(member);
                    }
                }
            }
        }
        res
    }

    pub fn input_loop(&mut self) {
        let mut abort = false;
        while !abort {
            if let Some(new_member) = Member::load_from_file(
                Path::new(&self.config.operating_directory),
                &self.members[self.selected_member].id,
            ) {
                self.members[self.selected_member] = new_member;
            }
            self.draw();
            let mut redraw = false;
            while !redraw && !abort {
                match crossterm::event::read() {
                    Ok(event) => match event {
                        Event::Key(key) => match self.input_mode {
                            InputMode::Normal => {
                                self.process_keys(&mut abort, &mut redraw, key);
                            }
                            InputMode::EnteringPoints => {
                                self.process_enter_points(key);
                                redraw = true;
                            }
                            InputMode::EnteringMembers => {
                                self.process_enter_members(key);
                                redraw = true;
                            }
                        },
                        Event::Mouse(_) => {}
                        Event::Resize(_, _) => {
                            redraw = true;
                        }
                    },
                    Err(err) => {
                        panic!("Error reading input event: {}", err)
                    }
                }
            }

            self.members[self.selected_member]
                .store_in_file(Path::new(&self.config.operating_directory));
        }

        disable_raw_mode().expect("Failed to disable raw mode");
        execute!(
            self.terminal.backend_mut(),
            crossterm::terminal::LeaveAlternateScreen,
        )
        .expect("Failed to leave alternate screen");
        self.terminal.show_cursor().expect("Failed to show cursor");
    }

    fn process_keys(&mut self, abort: &mut bool, redraw: &mut bool, key: KeyEvent) {
        if key.code == KeyCode::Esc
            || (key.code == KeyCode::Char('c') && key.modifiers == KeyModifiers::CONTROL)
        {
            *abort = true;
        }

        if self.messages.is_some() {
            if key.code == KeyCode::Enter {
                self.messages = None;
                *redraw = true;
            }
            return;
        }

        if key.code == KeyCode::Up {
            if key.modifiers == KeyModifiers::SHIFT {
                self.members[self.selected_member].decrement_selected_file();
            } else if key.modifiers == KeyModifiers::CONTROL {
                self.members[self.selected_member].decrement_output_file();
            } else {
                self.selected_member += self.members.len() - 1;
            }
            *redraw = true;
        }
        if key.code == KeyCode::Down {
            if key.modifiers == KeyModifiers::SHIFT {
                self.members[self.selected_member].increment_selected_file();
            } else if key.modifiers == KeyModifiers::CONTROL {
                self.members[self.selected_member].increment_output_file();
            } else {
                self.selected_member += 1;
            }
            *redraw = true;
        }
        self.selected_member %= self.members.len();
        if key.code == KeyCode::Char('r') {
            *redraw = true;
        }
        if key.code == KeyCode::Char('o') {
            self.members[self.selected_member].open_selected(&self.config);
            *redraw = true;
        }
        if key.code == KeyCode::Char('n') {
            *redraw = true;
        }
        if key.code == KeyCode::Char('p') {
            self.input_mode = InputMode::EnteringPoints;
            self.typed_string = "".to_string();
            *redraw = true;
        }
        if key.code == KeyCode::Char('a') {
            self.input_mode = InputMode::EnteringMembers;
            self.typed_string = "".to_string();
            *redraw = true;
        }
        if key.code == KeyCode::Char('x') {
            let member = &mut self.members[self.selected_member];
            if !member.other_members.is_empty() {
                member.other_members.remove(member.other_members.len() - 1);
            }
            *redraw = true;
        }
        if key.code == KeyCode::Char('e') {
            let messages = self.export();
            if !messages.is_empty() {
                self.messages = Some(messages);
            }
            *redraw = true;
        }
    }

    fn process_enter_points(&mut self, key: KeyEvent) {
        self.process_typed_string(key);
        if key.code == KeyCode::Esc {
            self.input_mode = InputMode::Normal;
            self.typed_string = "".to_string();
        }
        if key.code == KeyCode::Enter {
            if let Ok(points) = self.typed_string.parse::<f32>() {
                self.input_mode = InputMode::Normal;
                self.members[self.selected_member].points = Some(points);
                self.typed_string = "".to_string();
            }
        }
    }

    fn process_enter_members(&mut self, key: KeyEvent) {
        self.process_typed_string(key);
        if key.code == KeyCode::Up || key.code == KeyCode::Down {
            let member = &mut self.members[self.selected_member];
            let selected = member.get_suggested_persons(&self.typed_string, &self.point_sheets);
            if selected.len() > 0 {
                member.selected_name_option = member.selected_name_option.min(selected.len() - 1);
            }
            let delta = if key.code == KeyCode::Up {
                selected.len() - 1
            } else {
                1
            };
            member.selected_name_option += delta;
            member.selected_name_option %= selected.len();
        }
        if key.code == KeyCode::Esc {
            self.input_mode = InputMode::Normal;
            self.members[self.selected_member].selected_name_option = 0;
            self.typed_string = "".to_string();
        }
        if key.code == KeyCode::Enter || key.code == KeyCode::Tab {
            let member = &mut self.members[self.selected_member];
            let selected = member.get_suggested_persons(&self.typed_string, &self.point_sheets);
            if member.selected_name_option < selected.len() {
                member
                    .other_members
                    .push(selected[member.selected_name_option].clone());
            }

            self.input_mode = InputMode::Normal;
            member.selected_name_option = 0;
            self.typed_string = "".to_string();
        }
    }

    fn process_typed_string(&mut self, key: KeyEvent) {
        if let KeyCode::Char(c) = key.code {
            self.typed_string += &c.to_string();
        }
        if key.code == KeyCode::Backspace {
            let mut dist = 1;
            loop {
                if self.typed_string.len() >= dist {
                    if self
                        .typed_string
                        .is_char_boundary(self.typed_string.len() - dist)
                    {
                        break;
                    }
                    dist += 1;
                } else {
                    break;
                }
            }
            if self.typed_string.len() >= dist {
                self.typed_string = self.typed_string[..self.typed_string.len() - dist].to_string();
            }
        }
    }

    pub fn draw(&mut self) {
        self.members[self.selected_member].update(&self.config);
        self.terminal.draw(|f| {

            if let Some(messages) = &self.messages {
                let text = format!("Export Output (press Enter to continue): \n\n{}", messages);
                let paragraph = Paragraph::new(text.as_str());
                f.render_widget(paragraph, f.size());
                return;
            }


            // Draw member list:
            let header_layout = Layout::default().direction(Direction::Vertical).constraints([Constraint::Length(4), Constraint::Min(10)]).split(f.size());

            let header = Paragraph::new("Use [up]/[down] to navigate\nChanges are saved immediately\nAfter finishing correction for all groups, export with [e]");
            f.render_widget(header, header_layout[0]);

            let chunks = Layout::default()
                .direction(Direction::Horizontal)
                .margin(1)
                .constraints([Constraint::Length(30), Constraint::Min(30)])
                .split(header_layout[1]);
            let member_items: Vec<ListItem> = self
                .members
                .iter()
                .map(|m| ListItem::new(m.name.clone()))
                .collect();
            let members = List::new(member_items)
                .block(Block::default().title(" Groups ").borders(Borders::ALL))
                .style(Style::default().fg(Color::White))
                .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
                .highlight_symbol("> ");
            let mut member_state = ListState::default();
            member_state.select(Some(self.selected_member));
            f.render_stateful_widget(members, chunks[0], &mut member_state);

            self.members[self.selected_member].draw(
                f,
                chunks[1],
                self.input_mode,
                &self.typed_string,
                &self.point_sheets,
            );
        }).unwrap();
    }

    pub fn export(&self) -> String {
        let mut messages = "".to_string();

        let point_sheet_path = Path::new(&self.config.operating_directory).join("points.csv");
        let point_sheet = PointSheet::from_file(&point_sheet_path);

        let export_dir_name = Path::new(&self.config.operating_directory).join("export");

        let _ = fs::remove_dir_all(&export_dir_name);
        let _ = fs::create_dir(&export_dir_name);
        let tmp_dir = export_dir_name.join("tmp");
        fs::create_dir(&tmp_dir).unwrap();

        let point_file_lines = fs::read_to_string(&point_sheet_path)
            .unwrap()
            .lines()
            .map(|line| line.to_string())
            .collect::<Vec<_>>();
        let mut point_file = point_file_lines[0].to_string();

        for group in &self.members {
            for member in group.get_self_and_other_members() {
                // Find the current ID (the id changes on every point sheet (wtf moodle?!) and the
                // one stored in the member might be from an old point sheet:
                let id = point_sheet.get_id_of_person(&member).unwrap_or_else(|| {
                    panic!(
                        "Could not find {} (matriculation number {:?}) in current point sheet",
                        member.name, member.matriculation_number
                    )
                });

                // Copy the corrected pdf into output directory
                if let Some(output_file) = &group.output_file {
                    let output_file_path = group.directory.join(output_file);

                    let member_dir = export_dir_name
                        .join(format!("{}_{}_assignsubmission_file_", member.name, id));
                    let _ = fs::create_dir(&member_dir);

                    let tmp_file = tmp_dir.join("korrektur.pdf");
                    let tmp_res = tmp_dir.join("joined.pdf");
                    fs::copy(&output_file_path, &tmp_file).unwrap();
                    let mut pdf_tk_failure = false;

                    if !Path::new(&self.config.correction_note_file).exists() {
                        pdf_tk_failure = true;
                    } else {
                        let output = std::process::Command::new("pdftk")
                            .arg(&self.config.correction_note_file)
                            .arg(tmp_file.as_os_str())
                            .arg("cat")
                            .arg("output")
                            .arg(tmp_res.as_os_str())
                            .spawn();
                        if let Ok(mut output) = output {
                            match output.wait() {
                                Ok(status) => {
                                    if !status.success() {
                                        pdf_tk_failure = true;
                                        messages = format!(
                                            "{}Pdftk terminated with non-zero status code ({})\n",
                                            messages, status,
                                        );
                                    } else {
                                        fs::copy(&tmp_res, member_dir.join("korrektur.pdf"))
                                            .unwrap();
                                    }
                                }
                                Err(err) => {
                                    pdf_tk_failure = true;
                                    messages =
                                        format!("{}Pdftk failed with error ({})\n", messages, err,);
                                }
                            }
                        } else {
                            pdf_tk_failure = true;
                            messages = format!(
                                "{}Failed to launch pdftk. Raw file was copied instead.\n",
                                messages,
                            );
                        }
                    }

                    if pdf_tk_failure {
                        fs::copy(&output_file_path, member_dir.join("korrektur.pdf")).unwrap();
                    }
                } else {
                    messages = format!(
                        "{}Could not find output file for {}\n",
                        messages, group.name
                    );
                }

                // Add line with points to output point file
                if let Some(points) = group.points {
                    let search_pattern = format!("Teilnehmer/in{}", id);
                    let mut found_pattern = false;
                    for point_file_line in &point_file_lines {
                        if point_file_line.starts_with(&search_pattern) {
                            let new_row = Self::enter_points_into_row(point_file_line, points);
                            point_file = format!("{}\n{}", point_file, new_row);
                            found_pattern = true;
                        }
                    }
                    if !found_pattern {
                        messages = format!(
                            "{}No entry for {} (id: {}) in point file\n",
                            messages, member.name, id
                        );
                    }
                } else {
                    messages = format!("{}No points entered for {}\n", messages, group.name);
                }
            }
        }
        let _ = fs::remove_dir_all(&tmp_dir);

        fs::write(export_dir_name.join("points.csv"), point_file).unwrap();

        messages
    }

    pub fn enter_points_into_row(row: &str, points: f32) -> String {
        let mut comma_counter = 0;
        let mut inside_quotes = false;
        let mut inside_point_section = false;

        let mut resulting_string = "".to_string();

        for chr in row.chars() {
            if chr == '\"' {
                inside_quotes = !inside_quotes;
            }
            if chr == ',' && !inside_quotes {
                comma_counter += 1;
                if comma_counter == 4 {
                    inside_point_section = true;
                    let point_string = format!(",\"{:.2}\"", points);
                    resulting_string += &point_string;
                } else if comma_counter == 5 {
                    inside_point_section = false;
                }
            }
            if !inside_point_section {
                // It's very inefficient to build the string char by char, but if (hopefully)
                // avoids issues with indexing into a utf8 string
                resulting_string = format!("{}{}", resulting_string, chr);
            }
        }
        resulting_string
    }
}

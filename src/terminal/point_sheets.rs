use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::Path;

#[derive(Clone, Serialize, Deserialize)]
pub struct Person {
    pub id: String,
    pub name: String,
    pub matriculation_number: Option<String>, // Option since this is not known for the primary member
    pub certainty: f32,
}

impl Person {
    pub fn matches_search(&self, search: &str) -> bool {
        let elements = search.split(" ");

        let name_parts = self.name.split(' ').collect::<Vec<&str>>();
        let mut name_matched = vec![false; name_parts.len()];

        for element in elements {
            let non_capitals = element.chars().filter(|c| !c.is_uppercase()).count();
            if non_capitals == 0 {
                for search_letter in element.chars() {
                    let mut matched_letter = false;
                    for (i, name_part) in name_parts.iter().enumerate() {
                        if !name_matched[i] {
                            if name_part.chars().next().unwrap_or(' ') == search_letter {
                                name_matched[i] = true;
                                matched_letter = true;
                                break;
                            }
                        }
                    }
                    if !matched_letter {
                        return false;
                    }
                }
            } else {
                let lowercase_element = element.to_lowercase();
                let mut matched_part = false;
                for (i, name_part) in name_parts.iter().enumerate() {
                    if !name_matched[i] {
                        if name_part.to_lowercase().starts_with(&lowercase_element) {
                            name_matched[i] = true;
                            matched_part = true;
                            break;
                        }
                    }
                }
                if !matched_part {
                    return false;
                }
            }
        }

        true
    }
}

pub struct PointSheets {
    pub sheets: Vec<PointSheet>,
}

impl PointSheets {
    pub fn from_directory(path: &Path) -> Self {
        let mut sheets = Vec::new();
        for file in
            std::fs::read_dir(path).expect("Could not read contents of point sheet directory")
        {
            if let Ok(file) = file {
                sheets.push(PointSheet::from_file(&file.path()));
            }
        }

        Self { sheets }
    }

    pub fn identify_teammates(&self, own_name: &str) -> Vec<Person> {
        let mut map = HashMap::new();
        for sheet in &self.sheets {
            sheet.identify_teammates(own_name, &mut map);
        }
        let mut values = map
            .values()
            .map(|person| person.clone())
            .collect::<Vec<_>>();
        values.sort_by(|p_1, p_2| p_1.id.partial_cmp(&p_2.id).unwrap());
        values.sort_by(|p_1, p_2| p_2.certainty.partial_cmp(&p_1.certainty).unwrap());
        values
    }
}

pub struct PointSheet {
    entries: Vec<PointSheetEntry>,
}

impl PointSheet {
    pub fn from_file(path: &Path) -> Self {
        let mut entries = Vec::new();

        let mut csv = csv::Reader::from_path(path).expect("Failed to open point sheet");
        for record in csv.records() {
            let record = record.expect("Invalid record in point file");
            let id = record[0]
                .strip_prefix("Teilnehmer/in")
                .expect("ID does not correspond to expected format")
                .to_string();
            let name = record[1].to_string();
            let matriculation_number = record[2].to_string();
            let handed_in = !record[3].starts_with("Keine Abgabe");
            let grade = record[5].parse::<f32>().ok();

            let date_string = record[8].to_string();
            let (correction_day_of_month, correction_minutes_of_day) =
                if !date_string.is_empty() && date_string != "-" {
                    let len = date_string.len();
                    let minutes = date_string[len - 2..].parse::<u16>().unwrap();
                    let hours = date_string[len - 5..len - 3].parse::<u16>().unwrap();
                    let day_start = date_string.find(",").unwrap() + 2;
                    let day_end = date_string.find(".").unwrap();
                    let day = date_string[day_start..day_end].parse::<u8>().unwrap();
                    (Some(day), Some(hours * 24 + minutes))
                } else {
                    (None, None)
                };

            entries.push(PointSheetEntry {
                id,
                name,
                matriculation_number,
                handed_in,
                grade,
                correction_day_of_month,
                correction_minutes_of_day,
            })
        }
        Self { entries }
    }

    fn find_entry_for(&self, name: &str) -> Option<&PointSheetEntry> {
        self.entries.iter().find(|entry| entry.name == name)
    }

    pub fn identify_teammates(&self, own_name: &str, map: &mut HashMap<String, Person>) {
        let mut own_entry = self.find_entry_for(own_name);

        // Ugly hack because we want to use this function in the minitest module without having
        // a name
        let minitest_placeholder_entry = PointSheetEntry {
            id: "".to_string(),
            name: "".to_string(),
            matriculation_number: "".to_string(),
            handed_in: false,
            grade: None,
            correction_day_of_month: None,
            correction_minutes_of_day: None,
        };
        if own_name == "Minitest_Tool_Bypass" {
            own_entry = Some(&minitest_placeholder_entry)
        }
        if let Some(own_entry) = own_entry {
            for entry in &self.entries {
                if entry.id == own_entry.id {
                    continue;
                }

                let mut score = 0.0;
                if own_entry.handed_in && entry.handed_in {
                    score -= 2.0;
                }
                if own_entry.grade == entry.grade {
                    score += 1.0;
                }
                if let Some(delta_minutes) = own_entry.estimate_time_difference(entry) {
                    if delta_minutes < 10 {
                        score += 1.0 - (delta_minutes as f32 / 10.0);
                    }
                }

                if map.contains_key(&entry.name) {
                    map.get_mut(&entry.name).unwrap().certainty += score;
                } else {
                    map.insert(
                        entry.name.clone(),
                        Person {
                            id: entry.id.clone(),
                            name: entry.name.clone(),
                            matriculation_number: Some(entry.matriculation_number.clone()),
                            certainty: score,
                        },
                    );
                }
            }
        }
    }

    pub fn get_id_of_person(&self, person: &Person) -> Option<&str> {
        for entry in &self.entries {
            if entry.name == person.name {
                if let Some(matriculation_number) = &person.matriculation_number {
                    if &entry.matriculation_number == matriculation_number {
                        return Some(&entry.id);
                    }
                } else {
                    return Some(&entry.id);
                }
            }
        }
        None
    }
}

pub struct PointSheetEntry {
    id: String,
    name: String,
    matriculation_number: String,
    handed_in: bool,
    grade: Option<f32>,
    correction_day_of_month: Option<u8>,
    correction_minutes_of_day: Option<u16>,
}

impl PointSheetEntry {
    pub fn estimate_time_difference(&self, other: &Self) -> Option<u32> {
        let own_minutes_of_month =
            self.correction_day_of_month? as i32 * 1440 + self.correction_minutes_of_day? as i32;
        let other_minutes_of_month =
            other.correction_day_of_month? as i32 * 1440 + other.correction_minutes_of_day? as i32;
        Some((own_minutes_of_month - other_minutes_of_month).abs() as u32)
    }
}
